# Safe.js

#### 项目介绍

- 他极其轻巧，未经gzip压缩之前仅有5kb<br>
- 他极其快速，1ms的速度让构建大型项目成为可能<br>
- 他也极其优益，小小的身躯却有着大大的力量，能帮助你完成许多复杂的工作<br>
而他，就是 **"Safe.js"** 

#### 安装教程

1. 从码云上下载Safe.js
2. 使用```<script src='js/safe-min.js'></script>```引入Safe.js
3. 完成

#### 使用说明

如需获取使用说明请参考Safe.js的Wiki百科，地址：[safe.js的wiki](https://gitee.com/skyogo/Safe/wikis)

#### 参与贡献

欢迎参与safe.js的贡献！攻略：
1. Fork本项目
2. Star本项目
3. Watch本项目
4. 如有BUG欢迎在评论区留言！
